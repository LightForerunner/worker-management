/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package player;

import building.Building;
import javafx.scene.Group;
import resources.Resources;
import resources.Worker;

/**
 *
 * @author julinblanc
 */
public class Player {

    private final String namePlayer;
    private Group group;
    private Resources resources;
    private Piles piles;

    /**
     * Constructor of Player.
     *
     * @param namePlayer the name of the player.
     * @param group the group of the GameView
     */
    public Player(String namePlayer, Group group) {
        this.namePlayer = namePlayer;
        resources = new Resources();
        this.group = group;
        piles = new Piles(group, resources.getWorkers());
    }


    /**
     * Get the group.
     *
     * @return group
     */
    public Group getGroup() {
        return group;
    }
    

    /**
     * Get the piles
     *
     * @return piles
     */
    public Piles getPiles() {
        return piles;
    }

    /**
     * Get the resources.
     *
     * @return resources
     */
    public Resources getResources() {
        return resources;
    }

    /**
     * Get the name of the player.
     *
     * @return this name.
     */
    public String getName() {
        return namePlayer;
    }
    
    public void addWorker(Worker w){
        resources.getWorkers().add(w);
        piles.getWorkers().add(w);
    }

    /**
     * The player pays the resources required by the building production.
     * 
     * @param b the concerned building.
     */
    public void payResources(Building b) {
        getResources().addOrSubEnergies(b.getProduction().getEnergies().getQuantity());
        getResources().addOrSubMaterials(b.getProduction().getMaterials().getQuantity());
        getResources().addOrSubQuantity(b.getProduction().getQuantityWorkers());
    }

    /**
     * The player pays the resources required by the building production.
     * 
     * @param b the concerned building.
     */
    public void payCost(Building b) {
        getResources().addOrSubMaterials(-b.getMaterialCost());
    }

    /**
     * Checks if the player has the resources
     * 
     * @param b the building which would product.
     * 
     * @return has the player the resources required or not.
     */
    public boolean hasResources(Building b) {
        return (getResources().getEnergies().getQuantity() + b.getProduction().getEnergies().getQuantity() >= 0
                && getResources().getMaterials().getQuantity() + b.getProduction().getMaterials().getQuantity() >= 0
                && getResources().getQuantityWorkers() + b.getProduction().getQuantityWorkers() >= 0);
    }

    /**
     * Checks if the player has the materials
     * 
     * @param b the building than the player want to place.
     * 
     * @return has the player the materials required or not.
     */
    public boolean hasMaterials(Building b) {
        return (getResources().getMaterials().getQuantity() - b.getMaterialCost() >= 0);
    }
}
