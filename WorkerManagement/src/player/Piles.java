/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package player;

import building.*;
import display.GameView;
import display.LabelsViews;
import display.Tools;
import java.util.Random;
import java.util.Stack;
import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.control.Label;
import resources.Worker;

/**
 * The player's deck of workers and building.
 *
 * @author julinblanc && gdelleuse
 */
public class Piles implements LabelsViews {

    final private ArrayList<Building> buildsShadows;
    final private Worker workerShadow;
    private Stack<I_Block> iblocks;
    private Stack<J_Block> jblocks;
    private Stack<L_Block> lblocks;
    private Stack<O_Block> oblocks;
    private Stack<S_Block> sblocks;
    private Stack<T_Block> tblocks;
    private Stack<Z_Block> zblocks;
    private Stack<Worker> workers;
    private Label iQuantity, jQuantity,
            lQuantity, oQuantity, sQuantity,
            tQuantity, zQuantity, workerQuantity;

    /**
     * Constructor of BuildPile.
     *
     * @param group the list which contains all the elements in the interface.
     * @param workersPlayer the workers the player has.
     */
    public Piles(Group group, ArrayList<Worker> workersPlayer) {
        buildsShadows = new ArrayList<>();
        workerShadow = new Worker(group);
        initializeStacks(workersPlayer);
        pilesDisplay(group);
        initializeBuildPile(group);
        initializeLabels(group);
    }

    /**
     * Initialises the labels.
     *
     * @param group the list which contains all the elements in the interface.
     */
    private void initializeLabels(Group group) {
        iQuantity = new Label("" + iblocks.size());
        setLabel(group, iQuantity, 1.5);
        jQuantity = new Label("" + jblocks.size());
        setLabel(group, jQuantity, 5);
        lQuantity = new Label("" + lblocks.size());
        setLabel(group, lQuantity, 10);
        oQuantity = new Label("" + oblocks.size());
        setLabel(group, oQuantity, 14);
        sQuantity = new Label("" + sblocks.size());
        setLabel(group, sQuantity, 17.5);
        tQuantity = new Label("" + tblocks.size());
        setLabel(group, tQuantity, 21);
        zQuantity = new Label("" + zblocks.size());
        setLabel(group, zQuantity, 24.5);
        workerQuantity = new Label("" + workers.size());
        setLabel(group, workerQuantity, (Tools.WIDTH + 3 * Tools.rectSide) / 2,
                workerShadow.getDisplay().getRadius() / 4 + Tools.HEIGHT / 1.3);
    }

    /**
     * Set a pile's label.
     *
     * @param group the list which contains all the elements in the interface.
     * @param blockQuantity the label we want to set.
     * @param yPosFactor the factor which multiply the Y coord.
     */
    private void setLabel(Group group, Label blockQuantity, double yPosFactor) {
        blockQuantity.relocate(Tools.rectSide * 5.5, yPosFactor * Tools.rectSide);
        labelSetting(blockQuantity, 20);
        group.getChildren().add(blockQuantity);
    }

    /**
     * Set precisely where to set a pile's label.
     *
     * @param group the list which contains all the elements in the interface.
     * @param blockQuantity the label we want to set.
     * @param xPos the X position.
     * @param yPos the Y position.
     */
    private void setLabel(Group group, Label blockQuantity, double xPos, double yPos) {
        blockQuantity.relocate(xPos, yPos);
        labelSetting(blockQuantity, 20);
        group.getChildren().add(blockQuantity);
    }

    /**
     * Initialises all the stacks.
     */
    private void initializeStacks(ArrayList<Worker> workersPlayer) {
        iblocks = new Stack<>();
        jblocks = new Stack<>();
        lblocks = new Stack<>();
        oblocks = new Stack<>();
        sblocks = new Stack<>();
        tblocks = new Stack<>();
        zblocks = new Stack<>();
        workers = new Stack<>();
        workersPlayer.forEach(w -> workers.add(w));
    }

    /**
     * Displays the shadow pile's slot.
     *
     * @param group the list which contains all the elements in the interface.
     */
    private void pilesDisplay(Group group) {
        I_Block iSlot = new I_Block(group);
        buildsShadows.add(iSlot);
        J_Block jSlot = new J_Block(group);
        buildsShadows.add(jSlot);
        L_Block lSlot = new L_Block(group);
        buildsShadows.add(lSlot);
        O_Block oSlot = new O_Block(group);
        buildsShadows.add(oSlot);
        S_Block sSlot = new S_Block(group);
        buildsShadows.add(sSlot);
        T_Block tSlot = new T_Block(group);
        buildsShadows.add(tSlot);
        Z_Block zSlot = new Z_Block(group);
        buildsShadows.add(zSlot);
    }

    /**
     * Initialises each player's pile.
     *
     * @param group the list which contains all the elements in the interface.
     */
    private void initializeBuildPile(Group group) {
        Random r = new Random();
        Building initBuild;
        int idBuild;
        for (int i = 0; i < 5; i++) {
            idBuild = r.nextInt(7);
            switch (idBuild) {
                case 0:
                    initBuild = new I_Block();
                    iblocks.add((I_Block) initBuild);
                    break;
                case 1:
                    initBuild = new J_Block();
                    jblocks.add((J_Block) initBuild);
                    break;
                case 2:
                    initBuild = new L_Block();
                    lblocks.add((L_Block) initBuild);
                    break;
                case 3:
                    initBuild = new O_Block();
                    oblocks.add((O_Block) initBuild);
                    break;
                case 4:
                    initBuild = new S_Block();
                    sblocks.add((S_Block) initBuild);
                    break;
                case 5:
                    initBuild = new T_Block();
                    tblocks.add((T_Block) initBuild);
                    break;
                case 6:
                    initBuild = new Z_Block();
                    zblocks.add((Z_Block) initBuild);
                    break;
                default:
                    System.out.println("Random Int out of bonds.");
            }
        }

    }

    /**
     * Generates a random new building.
     *
     * @param group the list which contains all the elements in the interface.
     */
    public void generateNewBuilding(Group group) {
        Random r = new Random();
        boolean added = false;
        Building newBuild;
        int idBuild;
        while (!added) {
            idBuild = r.nextInt(7);
            if (isPilesFull()) {
                added = true;
                System.out.println("All your building slots have the maximum\n"
                        + "amount of buildings.");
            } else {
                switch (idBuild) {
                    case 0:
                        if (iblocks.size() < I_Block.getMAXNUMBER()) {
                            newBuild = new I_Block();
                            iblocks.add((I_Block) newBuild);
                            added = true;
                            System.out.println("You get " + newBuild.getBuildClass() + ".");
                        }
                        break;
                    case 1:
                        if (jblocks.size() < J_Block.getMAXNUMBER()) {
                            newBuild = new J_Block();
                            jblocks.add((J_Block) newBuild);
                            added = true;
                            System.out.println("You get " + newBuild.getBuildClass() + ".");
                        }
                        break;
                    case 2:
                        if (lblocks.size() < L_Block.getMAXNUMBER()) {
                            newBuild = new L_Block();
                            lblocks.add((L_Block) newBuild);
                            added = true;
                            System.out.println("You get " + newBuild.getBuildClass() + ".");
                        }
                        break;
                    case 3:
                        if (oblocks.size() < O_Block.getMAXNUMBER()) {
                            newBuild = new O_Block();
                            oblocks.add((O_Block) newBuild);
                            added = true;
                            System.out.println("You get " + newBuild.getBuildClass() + ".");
                        }
                        break;
                    case 4:
                        if (sblocks.size() < S_Block.getMAXNUMBER()) {
                            newBuild = new S_Block();
                            sblocks.add((S_Block) newBuild);
                            added = true;
                            System.out.println("You get " + newBuild.getBuildClass() + ".");
                        }
                        break;
                    case 5:
                        if (tblocks.size() < T_Block.getMAXNUMBER()) {
                            newBuild = new T_Block();
                            tblocks.add((T_Block) newBuild);
                            added = true;
                            System.out.println("You get " + newBuild.getBuildClass() + ".");
                        }
                        break;
                    case 6:
                        if (zblocks.size() < Z_Block.getMAXNUMBER()) {
                            newBuild = new Z_Block();
                            zblocks.add((Z_Block) newBuild);
                            added = true;
                            System.out.println("You get " + newBuild.getBuildClass() + ".");
                        }
                        break;
                    default:
                        System.out.println("Random Int out of bonds.");
                        added = true;
                }
            }
        }
        updateLabels();
    }

    /**
     * Checks if all the piles are full.
     *
     * @return is the piles are full or not.
     */
    private boolean isPilesFull() {
        return iblocks.size() == I_Block.getMAXNUMBER()
                && jblocks.size() == J_Block.getMAXNUMBER()
                && lblocks.size() == L_Block.getMAXNUMBER()
                && oblocks.size() == O_Block.getMAXNUMBER()
                && sblocks.size() == S_Block.getMAXNUMBER()
                && tblocks.size() == T_Block.getMAXNUMBER()
                && zblocks.size() == Z_Block.getMAXNUMBER();
    }

    /**
     * Getter of the buildings shadow attribute.
     *
     * @return the buildings shadows.
     */
    public ArrayList<Building> getBuildsShadows() {
        return buildsShadows;
    }

    /**
     * Getter of the worker shadow attribute.
     *
     * @return the worker shadow.
     */
    public Worker getWorkerShadow() {
        return workerShadow;
    }

    /**
     * Getter of the workers attribute.
     *
     * @return the workers.
     */
    public Stack<Worker> getWorkers() {
        return workers;
    }

    /**
     * Updates all the pile's labels.
     */
    @Override
    public void updateLabels() {
        iQuantity.setText("" + iblocks.size());
        jQuantity.setText("" + jblocks.size());
        lQuantity.setText("" + lblocks.size());
        oQuantity.setText("" + oblocks.size());
        sQuantity.setText("" + sblocks.size());
        tQuantity.setText("" + tblocks.size());
        zQuantity.setText("" + zblocks.size());
        workerQuantity.setText("" + workers.size());
    }

    /**
     * Selects a building and makes it as the game's current building.
     *
     * @param b the concerned building.
     * @param game the current game.
     */
    public void select(Building b, GameView game) {
        b.getSlots().forEach(s -> s.setOnMouseMoved(move -> {
            if (game.getCountDiscardBuilding() == 0) {
                s.setOnMouseClicked(e -> {
                    currentAssignment(b, game);
                    game.updateLabels();
                });
            } else {
                s.setOnMouseClicked(e -> {
                    s.setOnMouseClicked(f -> {
                        if (notEmpty(b)) {
                            System.out.println("You discard : " + b.getBuildClass() + ".");
                            game.addOrSubCountDiscardBuilding(-1);
                            System.out.println(game.getCountDiscardBuilding());
                            remove(b);
                            generateNewBuilding(game.getGroup());
                            game.updateLabels();
                            if (game.getCountDiscardBuilding() == 0) {
                                game.displayActionCount();
                            }
                        }
                    });
                });
            }
        }));
    }

    /**
     * Selects a worker and makes it as the game's current worker.
     *
     * @param game
     */
    public void select(GameView game) {
        workerShadow.getDisplay().setOnMouseClicked(e
                -> currentAssignment(game));
    }

    /**
     * Assigns a building as the game's current building.
     *
     * @param b the concerned building.
     * @param game the current game.
     */
    private void currentAssignment(Building b, GameView game) {
        if (b.getClass().getName().substring(9).equals("I_Block")
                && !iblocks.isEmpty()) {
            iblocks.peek().displayBuilding(game.getGraphicsContext(),
                    game.getGraphicsContext().getCanvas().getWidth() / 2.5,
                    game.getGraphicsContext().getCanvas().getHeight() / 2.5);
            game.setCurrentBuild(iblocks.peek());
        } else if (b.getClass().getName().substring(9).equals("J_Block")
                && !jblocks.isEmpty()) {
            jblocks.peek().displayBuilding(game.getGraphicsContext(),
                    game.getGraphicsContext().getCanvas().getWidth() / 2.5,
                    game.getGraphicsContext().getCanvas().getHeight() / 2.5);
            game.setCurrentBuild(jblocks.peek());
        } else if (b.getClass().getName().substring(9).equals("L_Block")
                && !lblocks.isEmpty()) {
            lblocks.peek().displayBuilding(game.getGraphicsContext(),
                    game.getGraphicsContext().getCanvas().getWidth() / 2.5,
                    game.getGraphicsContext().getCanvas().getHeight() / 2.5);
            game.setCurrentBuild(lblocks.peek());
        } else if (b.getClass().getName().substring(9).equals("O_Block")
                && !oblocks.isEmpty()) {
            oblocks.peek().displayBuilding(game.getGraphicsContext(),
                    game.getGraphicsContext().getCanvas().getWidth() / 2.5,
                    game.getGraphicsContext().getCanvas().getHeight() / 2.5);
            game.setCurrentBuild(oblocks.peek());
        } else if (b.getClass().getName().substring(9).equals("S_Block")
                && !sblocks.isEmpty()) {
            sblocks.peek().displayBuilding(game.getGraphicsContext(),
                    game.getGraphicsContext().getCanvas().getWidth() / 2.5,
                    game.getGraphicsContext().getCanvas().getHeight() / 2.5);
            game.setCurrentBuild(sblocks.peek());
        } else if (b.getClass().getName().substring(9).equals("T_Block")
                && !tblocks.isEmpty()) {
            tblocks.peek().displayBuilding(game.getGraphicsContext(),
                    game.getGraphicsContext().getCanvas().getWidth() / 2.5,
                    game.getGraphicsContext().getCanvas().getHeight() / 2.5);
            game.setCurrentBuild(tblocks.peek());
        } else if (b.getClass().getName().substring(9).equals("Z_Block")
                && !zblocks.isEmpty()) {
            zblocks.peek().displayBuilding(game.getGraphicsContext(),
                    game.getGraphicsContext().getCanvas().getWidth() / 2.5,
                    game.getGraphicsContext().getCanvas().getHeight() / 2.5);
            game.setCurrentBuild(zblocks.peek());
        } else {
            System.out.println("There are not buildings in this pile.");
        }
    }

    /**
     * Assign the current Worker.
     *
     * @param game the current game.
     */
    private void currentAssignment(GameView game) {
        if (!workers.isEmpty()) {
            game.setCurrentWorker(workers.peek());
        }
    }

    /**
     * Removes the building from the good pile.
     *
     * @param b the concerned building.
     */
    public void remove(Building b) {
        switch (b.getClass().getName().substring(9)) {
            case "I_Block":
                iblocks.pop();
                break;
            case "J_Block":
                jblocks.pop();
                break;
            case "L_Block":
                lblocks.pop();
                break;
            case "O_Block":
                oblocks.pop();
                break;
            case "S_Block":
                sblocks.pop();
                break;
            case "T_Block":
                tblocks.pop();
                break;
            case "Z_Block":
                zblocks.pop();
                break;
            default:
                System.out.println("Class Error: not found the building class.");
                break;
        }
        updateLabels();
    }

    /**
     * Checks if the appropriate pile has at least one building.
     *
     * @param b the concerned building slot.
     *
     * @return if the appropriate pile has at least one building or not.
     */
    public boolean notEmpty(Building b) {
        switch (b.getClass().getName().substring(9)) {
            case "I_Block":
                return (!iblocks.isEmpty());
            case "J_Block":
                return (!jblocks.isEmpty());
            case "L_Block":
                return (!lblocks.isEmpty());
            case "O_Block":
                return (!oblocks.isEmpty());
            case "S_Block":
                return (!sblocks.isEmpty());
            case "T_Block":
                return (!tblocks.isEmpty());
            case "Z_Block":
                return (!zblocks.isEmpty());
            default:
                return false;
        }
    }
}
