/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workermanagement;

import display.GameView;
import display.HelpView;
import display.MenuView;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author Juline Blanc && Gabriel Delleuse
 */
public class WorkerManagement extends Application {

    private MenuView menu;
    private GameView game;
    private HelpView help;

    @Override
    public void start(Stage primaryStage) {
        menu = new MenuView();
        help = new HelpView();
        game = new GameView();

        menu.setOnExitAction();
        menu.setOnHelpAction(()
                -> primaryStage.setScene(help.getScene())
        );
        menu.setOnPlayAction(()
                -> primaryStage.setScene(game.getScene())
        );
        help.setOnExitAction(()
                -> primaryStage.setScene(menu.getScene())
        );
        menu.setOnNewPlayAction(() -> {
            newPlay();
            primaryStage.setScene(game.getScene());
            game.setOnExitAction(()
                    -> primaryStage.setScene(menu.getScene())
            );
            game.setOnEndTurnAction(()
                    -> game.endTurn()
            );
            game.setOnRotateAction(()
                    -> game.rotateCurrentBuild()
            );
        });
        game.setOnExitAction(()
                -> primaryStage.setScene(menu.getScene())
        );
        game.setOnEndTurnAction(()
                -> game.endTurn()
        );
        game.setOnRotateAction(()
                -> game.rotateCurrentBuild()
        );
        //Sets the window, and display it
        primaryStage.setTitle("Worker Management");
        primaryStage.setScene(menu.getScene());
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args); //Launcher
    }

    /**
     * Makes a new game.
     *
     */
    public void newPlay() {
        game.getGroup().getChildren().clear();
        game = new GameView();
    }
}
