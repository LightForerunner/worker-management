/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author Gabriel Delleuse
 */
public class Resources {

	private Material materials;
	private Energy energies;
	private ArrayList<Worker> workers;

	/**
	 * Constructor of Ressources.
	 */
	public Resources() {
		this.materials = new Material(8);
		this.energies = new Energy(16);
		this.workers = Worker.Worker(8);
	}

	/**
	 * Another constructor.
	 * @param materials the number of materials.
	 * @param energies the number of energies.
	 * @param workers the number of workers.
	 */
	public Resources(int materials, int energies, int workers) {
		this.materials = new Material(materials);
		this.energies = new Energy(energies);
		this.workers = Worker.Worker(workers);
	}

	/**
	 * Get the materials.
	 *
	 * @return materials.
	 */
	public Material getMaterials() {
		return materials;
	}

	/**
	 * Modify the materials.
	 *
	 * @param x the amount of materials we want to add or substract.
	 */
	public void addOrSubMaterials(int x) {
		this.materials.addOrSubQuantity(x);
	}

	/**
	 * Get the energies.
	 *
	 * @return energies
	 */
	public Energy getEnergies() {
		return energies;
	}

	/**
	 * Modify the energies.
	 *
	 * @param x the amount of energies we want to add or substract.
	 */
	public void addOrSubEnergies(int x) {
		this.energies.addOrSubQuantity(x);
	}

	/**
	 * Get the quantity of workers.
	 * @return the quantity.
	 */
	public int getQuantityWorkers() {
		return workers.size();
	}

	/**
	 * Get the workers.
	 *
	 * @return an ArrayList workers.
	 */
	public ArrayList<Worker> getWorkers() {
		return workers;
	}

	/**
	 * Display the workers.
	 * @param group the list which contains all the elements in the interface.
	 * @param x x-ccordinate.
	 * @param y y-coordinate.
	 */
	public void displayWorkers(Group group, double x, double y) {
		Circle c;
		int space = 0;
		for (Worker w : workers) {
			w.getDisplay().relocate(x + space + 10, y);
			w.getDisplay().setFill(Color.CORAL);
			w.getDisplay().setStroke(Color.BLACK);
			group.getChildren().add(w.getDisplay());
			space += 30;
		}
	}

	/**
	 * Modify the quantity attribute by adding or substracting.
	 *
	 * @param quantity the quantity to modify.
	 */
	public void addOrSubQuantity(int quantity) {
		if (quantity > 0) {
			for (int i = 0; i < quantity; i++) {
				this.workers.add(new Worker());
			}
		} else if (quantity < 0) {
			for (int i = 0; i > quantity; i--) {
				this.workers.remove(workers.size() - 1);
			}
		}
	}
}
