/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import building.Building;
import display.GameView;
import display.Slot;
import display.Tools;
import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import player.Piles;

/**
 *
 * @author Gabriel Delleuse
 */
public class Worker {

    private Building build;
    private Circle display;

    /**
     * Constructor of Worker.
     */
    public Worker() {
        build = null;
    }

    public Worker(Group group) {
        build = null;
        display = new Circle(Tools.rectSide / 2);
        display.setStroke(Color.WHITE);
        display.setFill(Tools.DARKGREY);
        display.relocate((Tools.WIDTH - Tools.rectSide) / 2, Tools.HEIGHT / 1.3);
        group.getChildren().add(display);
    }

    /**
     * Get the building.
     *
     * @return the building.
     */
    public Building getBuild() {
        return build;
    }

    /**
     * Set the building attribute.
     *
     * @param build the building to set.
     */
    public void setBuild(Building build) {
        this.build = build;
    }

    /**
     * Get the display.
     *
     * @return a circle.
     */
    public Circle getDisplay() {
        return display;
    }

    /**
     * Set the display.
     *
     * @param display the circle.
     */
    public void setDisplay(Circle display) {
        this.display = display;
    }

    /**
     * Constructor of Worker.
     *
     * @param quantity the quantity of workers.
     * @return a list of workers.
     */
    public static ArrayList<Worker> Worker(int quantity) {
        ArrayList<Worker> workers = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            workers.add(new Worker());
        }
        return workers;
    }

    /**
     * Place a worker.
     *
     * @param s the slot.
     * @param game
     * @param pile
     */
    public void placement(Slot s, GameView game, Piles pile) {
        if (s.getBuild() != null && s.getWorker() == null) {
            System.out.println("Worker Placed");
            s.setWorker(this);
            s.getBuild().getWorkerSlots().add(this);
            display = new Circle(Tools.rectSide / 2);
            display.setStroke(Color.BLACK);
            display.setFill(Color.YELLOW);
            display.relocate(s.getX(), s.getY());
            game.getGroup().getChildren().add(display);
            game.addOrSubCountActionWorker(-1);
            game.getPlayer().getResources().addOrSubQuantity(-1);
            game.updateLabels();
            game.setCurrentWorkerNull();
            game.getPlayer().getPiles().getWorkers().pop();
            game.getPlayer().getPiles().updateLabels();
        }
    }
    
    /**
     * Place a worker.
     *
     * @param s the slot.
     * @param game
     * @param pile
     */
    public void relocateAndPlace(Slot s, GameView game, Piles pile) {
        if (s.getBuild() != null && s.getWorker() == null) {
            System.out.println("Worker Moved");
            s.setWorker(this);
            s.getBuild().getWorkerSlots().add(this);
            display.relocate(s.getX(), s.getY());
            game.addOrSubCountFreelyMoveWorker(-1);
            game.getPlayer().getResources().addOrSubQuantity(-1);
            game.updateLabels();
            game.setCurrentWorkerNull();
            game.getPlayer().getPiles().getWorkers().pop();
            game.getPlayer().getPiles().updateLabels();
        }
    }
}
