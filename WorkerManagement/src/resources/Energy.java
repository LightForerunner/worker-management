/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

/**
 *
 * @author Gabriel Delleuse
 */
public class Energy {
    private int quantity;

    /**
     * Constructor of Energy.
     * 
     * @param quantity 
     */
    public Energy(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Get the quantity.
     * 
     * @return quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Modify the quantity attribute by adding or substracting.
     * 
     * @param quantity the quantity to modify.
     */
    public void addOrSubQuantity(int quantity) {
        this.quantity += quantity;
    }
}
