package display;

import building.Building;
import java.util.ArrayList;
import java.util.HashSet;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import resources.Worker;

/**
 * The game board object class.
 *
 * @author julinblanc && gdelleuse
 */
public class GameBoard {

    private final ArrayList<Slot> slots;
    private final HashSet<Building> builds;
    private final int rows, cols;
    private int countBuilding;

    /**
     * Constructor of GameBoard.
     */
    public GameBoard() {
        slots = new ArrayList<>();
        builds = new HashSet<>();
        rows = 20;
        cols = 10;
    }

    /**
     * Get the rows.
     *
     * @return the number of rows.
     */
    public int getRows() {
        return rows;
    }

    /**
     * Get the slots.
     *
     * @return an arrayList of slots.
     */
    public ArrayList<Slot> getSlots() {
        return slots;
    }

    /**
     * Get the buildings.
     *
     * @return a Hashset of Building.
     */
    public HashSet<Building> getBuilds() {
        return builds;
    }

    /**
     * Selects a worker and makes it as the game's current worker.
     *
     * @param game
     */
    public void select(GameView game) {
        getSlots().forEach(s -> {
            s.setOnMouseMoved(e -> {
                if (s.hasWorker() && game.getCountFreelyMoveWorker() > 0) {
                    s.getWorker().getDisplay().setOnMouseClicked(f
                            -> s.currentAssignement(game));
                }
            });
        });
    }

    /**
     * Creates the grid.
     *
     * @param group the list which contains all the elements in the interface.
     */
    public void gridCreation(Group group) {
        Slot rect;
        for (int i = 0; i < cols; i++) {
            for (int j = 0; j < rows; j++) {
                rect = new Slot(i * Tools.rectSide + ((Tools.WIDTH - cols * Tools.rectSide) / 2),
                        j * Tools.rectSide + (int) Tools.HEIGHT / 50, Tools.rectSide, Tools.rectSide);
                rect.setFill(Color.WHITE);
                rect.setStroke(Color.BLACK);
                group.getChildren().add(rect);
                slots.add(rect);
            }
        }
    }

    /**
     * Detect when the mouse is on the board and place if a click is detected.
     *
     * @param b the current building
     * @param s the current slot
     * @param game
     */
    public void containsPlaceAndRemove(Building b,
            Slot s, GameView game) {
        if (b != null && game.getCountPlaceBuilding() > 0) {
            b.placement(s, game);
            b = null;
        }
    }

    /**
     * Detect when the mouse is on the board and place the worker if a click is
     * detected on a placed Building.
     *
     * @param w the current Worker
     * @param s the current slot
     * @param game
     */
    public void containsPlaceAndRemoveWorker(Worker w,
            Slot s, GameView game) {
        Slot slot;
        if (w != null) {
            if (game.getCountFreelyMoveWorker() > 0 && findSlot(w) != null) {
                slot = findSlot(w);
                slot.setWorkerNull();
                w.relocateAndPlace(s, game, game.getPlayer().getPiles());
                w = null;
            } else {
                w.placement(s, game, game.getPlayer().getPiles());
                w = null;
            }
        }
    }

    /**
     * Verify if the building can be placed in a certain space.
     *
     * @param zero represents the first slot.
     * @param first represents the second slot.
     * @param second represents the third slot.
     * @param third represents the fourth slot.
     * @return if it can be possible.
     */
    public boolean buildIsNullAndNotOutOfBonds(int zero, int first, int second, int third) {
        boolean isInOrInVoid = true;
        if (0 > zero || zero > slots.size() - 1) {
            isInOrInVoid = false;
        } else if (0 > first || first > slots.size() - 1) {
            isInOrInVoid = false;
        } else if (0 > second || second > slots.size() - 1) {
            isInOrInVoid = false;
        } else if (0 > third || third > slots.size() - 1) {
            isInOrInVoid = false;
        } else if (slots.get(zero).hasBuild()) {
            isInOrInVoid = false;
        } else if (slots.get(first).hasBuild()) {
            isInOrInVoid = false;
        } else if (slots.get(second).hasBuild()) {
            isInOrInVoid = false;
        } else if (slots.get(third).hasBuild()) {
            isInOrInVoid = false;
        }
        if (!isInOrInVoid) {
            System.out.println("There is a build here, or out of bonds :\nPlease place the build in an appropriate place.");
        }
        return isInOrInVoid;
    }

    /**
     * Verify if the building can be place vertically.
     *
     * @param zero represents the first slot.
     * @param first represents the second slot.
     * @param second represents the third slot.
     * @param third represents the fourth slot.
     * @return if it can be possible.
     */
    public boolean isNotOutOfVerticalBonds(int zero, int first, int second, int third) {
        boolean isInVerticalBonds = true;
        if (buildIsNullAndNotOutOfBonds(zero, first, second, third)) {
            if (isOutOfTopOrBottom(zero, first, second, third)) {
                isInVerticalBonds = false;
            } else if (isOutOfTopOrBottom(first, zero, second, third)) {
                isInVerticalBonds = false;
            } else if (isOutOfTopOrBottom(second, zero, first, third)) {
                isInVerticalBonds = false;
            } else if (isOutOfTopOrBottom(third, zero, first, second)) {
                isInVerticalBonds = false;
            }
            if (!isInVerticalBonds) {
                System.out.println("Not enough vertical space.");
            }
        } else {
            isInVerticalBonds = false;
            System.out.println("");
        }
        return isInVerticalBonds;
    }

    /**
     * Verify if the building is out on the top or the bottom.
     *
     * @param zero represents the first slot.
     * @param first represents the second slot.
     * @param second represents the third slot.
     * @param third represents the fourth slot.
     * @return if the slots are in wel coords.
     */
    private boolean isOutOfTopOrBottom(int zero, int first, int second, int third) {
        return ((zero - (rows - 1)) % rows == 0 && (first % rows == 0
                || second % rows == 0
                || third % rows == 0))
                || (zero % rows == 0 && ((first - (rows - 1)) % rows == 0
                || (second - (rows - 1)) % rows == 0
                || (third - (rows - 1)) % rows == 0));
    }

    /**
     * Finds the slot which contains the worker.
     *
     * @param w the worker.
     * @return the slot which has the worker in parameter
     */
    public Slot findSlot(Worker w) {
        for (Slot s : slots) {
            if (s.hasWorker()) {
                if (s.getWorker().equals(w)) {
                    return s;
                }
            }
        }
        return null;
    }
}
