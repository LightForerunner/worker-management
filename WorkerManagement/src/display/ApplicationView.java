/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package display;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author gdelleuse
 */
public interface ApplicationView {

    Scene getScene();

    /**
     * Sets the display of the button.
     *
     * @param b the button we set.
     * @param width the width of the button.
     * @param height the height of the button.
     * @param fontSize the size of the text in the button.
     */
    public default void buttonSetting(Button b, double width, double height, int fontSize) {
        b.setPrefSize(width, height);
        b.setStyle("-fx-border-color: rgb(255,255,255);"
                + " -fx-border-width: 5;"
                + " -fx-background-color: rgb(165, 165, 165);");
        b.setTextFill(Tools.WHITE);

        b.setFont(Font.font("Arial", FontWeight.BOLD, fontSize));
    }

	/**
	 * Set the effects when mouse is pressed on a button or released.
	 * @param b the button.
	 */
    default public void buttonEffect(Button b) {
        b.setOnMousePressed(event
                -> b.setStyle("-fx-border-color: rgb(255,255,255);"
                        + " -fx-border-width: 5;"
                        + " -fx-background-color: rgb(90, 90, 90);"));
        b.setOnMouseReleased(event
                -> b.setStyle("-fx-border-color: rgb(255,255,255);"
                        + " -fx-border-width: 5;"
                        + " -fx-background-color: rgb(165, 165, 165);"));
    }
}
