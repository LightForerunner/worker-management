/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package display;

import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author Juline Blanc && Gabriel Delleuse
 */
public class MenuView implements ApplicationView, LabelsViews {

    private final Scene scene;
    private final Label titleLabel;
    private final Button playButton;
    private final Button newPlayButton;
    private final Button helpButton;
    private final Button exitButton;

    /**
     * Constructor of MenuView.
     */
    public MenuView() {
        //initialization 
        playButton = new Button("Play!");
        newPlayButton = new Button("New Game");
        helpButton = new Button("How to play");
        exitButton = new Button("exit");
        titleLabel = new Label("WORKER MANAGEMENT");
        Group group = new Group(titleLabel, playButton, newPlayButton, helpButton, exitButton);
        scene = new Scene(group, Tools.WIDTH, Tools.HEIGHT);

        //Buttons setting
        buttonSetting(playButton, 650, 275, 50);
        playButton.relocate((Tools.WIDTH - playButton.getPrefWidth())/2,
                175);
        buttonEffect(playButton);
        buttonSetting(newPlayButton, 400, 75, 30);
        newPlayButton.relocate((Tools.WIDTH - newPlayButton.getPrefWidth())/2,
                475);
        buttonEffect(newPlayButton);
        buttonSetting(helpButton, 250, 50, 25);
        helpButton.relocate((Tools.WIDTH - helpButton.getPrefWidth())/2,
                575);
        buttonEffect(helpButton);
        buttonSetting(exitButton, 125, 30, 22);
        exitButton.relocate((Tools.WIDTH - exitButton.getPrefWidth())/2,
                655);
        buttonEffect(exitButton);

        //Label setting
        labelSetting(titleLabel, 65);
        titleLabel.relocate(140, 50);

        scene.setFill(Tools.DARKGREY);
    }

    /**
     * Get the scene.
     *
     * @return the scene.
     */
    @Override
    public Scene getScene() {
        return scene;
    }

    /**
     * Set action for the Play button.
     *
     * @param onPlayAction
     */
    public void setOnPlayAction(Runnable onPlayAction) {
        playButton.setOnAction(event -> onPlayAction.run());
    }

    /**
     * Set action for the New Play button.
     *
     * @param onNewPlayAction
     */
    public void setOnNewPlayAction(Runnable onNewPlayAction) {
        newPlayButton.setOnAction(event -> onNewPlayAction.run());
    }

    /**
     * Set action for the Help button.
     *
     * @param onHelpAction
     */
    public void setOnHelpAction(Runnable onHelpAction) {
        helpButton.setOnAction(event -> onHelpAction.run());
    }

    /**
     * Sets action for the Exit button : exit...
     *
     */
    public void setOnExitAction() {
        exitButton.setOnAction(event -> Platform.exit());
    }

    @Override
    public void updateLabels() {
        //Nothing to do.
    }
}
