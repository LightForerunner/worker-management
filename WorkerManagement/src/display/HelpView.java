package display;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author gdelleuse && julinblanc
 */
public class HelpView implements ApplicationView, LabelsViews {

    private final Button exitButton;
    private final Scene scene;
    private final Label titleLabel;
    private final Label helpLabel;
    private Runnable onExit = null;

    public HelpView() {
        //Initialization
        exitButton = new Button("return");
        titleLabel = new Label("WORKER MANAGEMENT");
        helpLabel = new Label(" ");

        buttonSetting(exitButton, 250, 50, 20);
        buttonEffect(exitButton);
        exitButton.relocate(Tools.WIDTH - exitButton.getPrefWidth() - 1,
                Tools.HEIGHT - exitButton.getPrefHeight() - 1);
        labelSetting(titleLabel, 65);
        titleLabel.relocate(140, 50);
        labelSetting(helpLabel, 20);
        helpLabel.relocate(75, 125);
        updateLabels();

        Group group = new Group(titleLabel, helpLabel, exitButton);
        scene = new Scene(group, Tools.WIDTH, Tools.HEIGHT);
        scene.setFill(Tools.DARKGREY);
        exitButton();
    }

    @Override
    public Scene getScene() {
        return scene;
    }

    @Override
    public void updateLabels() {
        helpLabel.setText("THE RULES :\n"
                + "     You'll start with 8 workers, 16 energies, 8 workers (these three as your resources) \n"
                + "and 5 random generated building.\n"
                + "     A building has an unique production depending of the type, and is a combinaison of 4 slots.\n"
                + "To have this production, you have to put a worker on a slot. Each worker on a building's\n"
                + "slot will perform this production, and consume the resources needed to perform it.\n\n"
                + "     Each turn, you can put one building on the board, and a worker. A worker\n"
                + "can only be put on a building slot. You can't move a worker in the board without the Park Building.");
    }

    /**
     * Active the setOnMousePressed and the changing color of the button.
     */
    private void exitButton() {
        buttonEffect(this.exitButton);
        exitButton.setOnAction(event -> onExit.run());
    }

    /**
     * Set the action while pressing the Exit button.
     *
     * @param onExit a button.
     */
    public void setOnExitAction(Runnable onExit) {
        this.onExit = onExit;
    }
}
