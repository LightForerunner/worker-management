/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package display;

import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author Gabriel Delleuse
 */
public interface LabelsViews {

    /**
     * Set the label.
     *
     * @param l the label.
     * @param sizeFont the size.
     */
    default public void labelSetting(Label l, int sizeFont) {
        l.setFont(Font.font("Arial", FontWeight.BOLD, sizeFont));
        l.setTextFill(Tools.WHITE);
        l.setEffect(Tools.LIGHTSHADOW);
    }

    /**
     * Update the labels.
     */
    void updateLabels();
}
