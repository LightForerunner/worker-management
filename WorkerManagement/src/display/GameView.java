package display;

import building.Building;
import java.util.HashMap;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import player.Player;
import resources.Worker;

/**
 * The view of the game and all the rules of it.
 *
 * @author julinblanc && gdelleuse
 */
public class GameView implements ApplicationView, LabelsViews {

    private final Button exitButton;
    private final Button endButton;
    private final Button rotateButton;
    private final GameBoard board;
    private final Group group;
    private final Scene scene;

    private final GraphicsContext gc;
    private final Label resourcesPlayer;
    private final Label resourcesBuilding;
    private final Label action;
    private final Player player;
    private Building currentBuild = null;
    private HashMap<Building, Worker> build;
    private Runnable onExit = null;
    private Worker currentWorker = null;
    private int countPlaceBuilding = 1;
    private int countDiscardBuilding = 0;
    private int countPlaceWorker = 1;
    private int countFreelyMoveWorker = 0;
    private int countTurn = 1;

    /**
     * Constructor of GameView.
     */
    public GameView() {
        //Initialization
        group = new Group();
        scene = new Scene(group, Tools.WIDTH, Tools.HEIGHT);
        exitButton = new Button("return");
        endButton = new Button("Skip turn");
        rotateButton = new Button("Rotate Selected\nButton");
        board = new GameBoard();
        player = new Player("Player 1", group);
        resourcesPlayer = new Label("Material : " + player.getResources().getMaterials().getQuantity()
                + "\nEnergy : " + player.getResources().getEnergies().getQuantity()
                + "\nWorker : " + player.getResources().getQuantityWorkers());
        action = new Label("This turn you can : \n"
                + "Place " + countPlaceBuilding + " building(s),\n"
                + "Place " + countPlaceWorker + " worker(s),\n"
                + "Freely move " + countFreelyMoveWorker + " worker(s) in \nthe board.");
        resourcesBuilding = new Label();

        //Buttons setting
        buttonSetting(exitButton, 250, 50, 20);
        buttonSetting(endButton, 250, 50, 20);
        buttonSetting(rotateButton, 250, 50, 20);
        exitButton.relocate(Tools.WIDTH - exitButton.getPrefWidth(),
                Tools.HEIGHT - exitButton.getPrefHeight() - 1);
        endButton.relocate(Tools.WIDTH - endButton.getPrefWidth(), 1);
        rotateButton.relocate(Tools.WIDTH - endButton.getPrefWidth(),
                Tools.HEIGHT / 2 - rotateButton.getPrefHeight() / 2);
        buttonEffect(endButton);
        buttonEffect(exitButton);
        buttonEffect(rotateButton);

        //Labels setting
        labelSetting(resourcesPlayer, 20);
        resourcesPlayer.relocate(Tools.WIDTH / 2.22, Tools.HEIGHT / 1.125);
        labelSetting(resourcesBuilding, 15);
        resourcesBuilding.relocate(Tools.WIDTH - endButton.getPrefWidth() + 5,
                75 + Tools.HEIGHT / 2 - rotateButton.getPrefHeight() / 2);
        labelSetting(action, 16);
        action.relocate(Tools.WIDTH - endButton.getPrefWidth() + 5,
                250 + Tools.HEIGHT / 2 - rotateButton.getPrefHeight() / 2);

        //Making the scene
        group.getChildren().add(resourcesPlayer);
        group.getChildren().add(resourcesBuilding);
        group.getChildren().add(action);
        group.getChildren().add(exitButton);
        group.getChildren().add(rotateButton);
        group.getChildren().add(endButton);
        Canvas c = new Canvas(250, 250);
        c.relocate(Tools.WIDTH - c.getWidth(), (Tools.HEIGHT / 2) - (rotateButton.getPrefHeight() / 2) - c.getHeight());
        gc = c.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, c.getWidth(), c.getHeight());
        group.getChildren().add(c);
        board.gridCreation(group);
        scene.setFill(Tools.DARKGREY);

        if (countTurn < 30) {
            player.getPiles().getBuildsShadows().forEach((b) -> {
                player.getPiles().select(b, this);
            });
            board.select(this);
            player.getPiles().select(this);
            board.getSlots()
                    .forEach((s) -> {
                        s.setOnMouseClicked(event
                                -> {
                            board.containsPlaceAndRemoveWorker(currentWorker, s, this);
                            board.containsPlaceAndRemove(currentBuild, s, this);
                        });
                    });
        } else {

        }

    }

    /**
     * Get the graphics context.
     *
     * @return the graphic.
     */
    public GraphicsContext getGraphicsContext() {
        return gc;
    }

    /**
     * Get the group.
     *
     * @return the group.
     */
    public Group getGroup() {
        return group;
    }

    /**
     * Get the current building.
     *
     * @return the building.
     */
    public Building getCurrentBuild() {
        return currentBuild;
    }

    /**
     * Get the board.
     *
     * @return the game board.
     */
    public GameBoard getBoard() {
        return board;
    }

    /**
     * Set the current building.
     *
     * @param currentBuild the building.
     */
    public void setCurrentBuild(Building currentBuild) {
        System.out.println("Building selected : " + currentBuild.getNameBuilding());
        this.currentBuild = currentBuild;
    }

    /**
     * Set the current Worker.
     *
     * @param currentWorker the worker.
     */
    public void setCurrentWorker(Worker currentWorker) {
        System.out.println("Worker selected.");
        this.currentWorker = currentWorker;
    }

    /**
     * Set the action while pressing the Exit button.
     *
     * @param onExit a button.
     */
    public void setOnExitAction(Runnable onExit) {
        this.onExit = onExit;
        exitButton.setOnAction(event -> onExit.run());
    }

    /**
     * Set the action while pressing the Rotate Button.
     *
     * @param rotateAction an action.
     */
    public void setOnRotateAction(Runnable rotateAction) {
        rotateButton.setOnAction(event -> rotateAction.run());
    }

    /**
     * Rotates the current build if not null.
     */
    public void rotateCurrentBuild() {
        if (currentBuild != null) {
            currentBuild.rotate(gc);
        } else {
            System.out.println("No selected building.");
        }
    }

    /**
     * Set the action while pressing the Skip Turn button.
     *
     * @param EndTurn an action.
     */
    public void setOnEndTurnAction(Runnable EndTurn) {
        endButton.setOnAction(event -> EndTurn.run());
    }

    /**
     * Set the current building equals null.
     */
    public void setCurrentBuildNull() {
        currentBuild = null;
    }

    /**
     * Set the current Worker equals null.
     */
    public void setCurrentWorkerNull() {
        currentWorker = null;
    }

    /**
     * Position of the mouse class.
     */
    private static final class MouseLocation {

        public double x, y;
    }

    /**
     * Get the scene.
     *
     * @return the scene.
     */
    @Override
    public Scene getScene() {
        return scene;
    }

    /**
     * Updates the ressource label to print the resources values.
     */
    @Override
    public void updateLabels() {
        resourcesPlayer.setText("Material : " + player.getResources().getMaterials().getQuantity()
                + "\nEnergy : " + player.getResources().getEnergies().getQuantity()
                + "\nWorker : " + player.getResources().getQuantityWorkers());
        if (currentBuild == null) {
            resourcesBuilding.setText("");
        } else {
            resourcesBuilding.setText(currentBuild.productionToString());
        }
        if (countDiscardBuilding == 0) {
            displayActionCount();
        } else {
            discardLabel();
        }
    }

    /**
     * Set the label action when the player has to discard a building.
     */
    public void discardLabel() {
        action.setText("You have to discard a building.\n"
                + "To do it, you have to double\n"
                + "click, or click twice on the pile\n"
                + "you want to discard the building.");
    }

    /**
     * Displays the default text for the action label.
     */
    public void displayActionCount() {
        action.setText("This turn you can : \n"
                + "Place " + countPlaceBuilding + " building(s),\n"
                + "Place " + countPlaceWorker + " worker(s),\n"
                + "Freely move " + countFreelyMoveWorker + " worker(s) in \nthe board.");
    }

    /**
     * Remove the building from the build pile, the board and the pile.
     *
     * @param b
     */
    private void remove(Building b) {
        b.getSlots().forEach(s -> {
            group.getChildren().remove(s);
            setCurrentBuildNull();
        });
    }

    /**
     * Add this in the game board builds and remove this from the game (except
     * from the game board).
     *
     * @param b
     */
    public void clearGame(Building b) {
        remove(b);
        board.getBuilds().add(b);
        player.getPiles().remove(b);
        setCurrentBuildNull();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
        updateLabels();
    }

    /**
     * Get the buildings on the board, and add or sub ressources.
     */
    public void endTurn() {
        int cptLBWorker = 0;
        countTurn++;
        countPlaceBuilding = 1;
        countPlaceWorker = 1;
        countFreelyMoveWorker = 0;
        for (Slot s : board.getSlots()) {
            if (s.hasWorker() && s.hasBuild()) {
                switch (s.getBuild().getBuildClass()) {
                    case "L_Block":
                        if (player.hasResources(s.getBuild())) {
                            s.getBuild().product(player);
                            s.setWorkerNull();
                        }
                        break;
                    case "O_Block":
                        countFreelyMoveWorker++;
                        break;
                    case "Z_Block":
                        countDiscardBuilding++;
                        break;
                    default:
                        s.getBuild().product(player);
                        break;
                }
            }
        }
        if (player.getPiles().getBuildsShadows().isEmpty()) {
            player.getPiles().generateNewBuilding(group);
        }
        player.getPiles().updateLabels();
        updateLabels();
    }

    /**
     * The concerned Worker becomes the currentWorker.
     *
     * @param w the worker selected.
     */
    public void select(Worker w) {
        w.getDisplay().setOnMouseClicked(event
                -> {
            currentWorker = w;
        });
    }

    /**
     * Get the number of placement of worker left.
     *
     * @return the number of actions.
     */
    public int getCountPlaceWorker() {
        return countPlaceWorker;
    }

    /**
     * Get the number of placement of worker left.
     *
     * @param value the value we add
     */
    public void addOrSubCountActionWorker(int value) {
        countPlaceWorker += value;
    }

    /**
     * Decrements the turn.
     */
    public void decrementPlaceBuilding() {
        countPlaceBuilding--;
    }

    /**
     * Get the number of building's placement left.
     *
     * @return the number of actions.
     */
    public int getCountPlaceBuilding() {
        return countPlaceBuilding;
    }

    /**
     * Get how much time the player has to discard a building.
     *
     * @return the number of actions.
     */
    public int getCountDiscardBuilding() {
        return countDiscardBuilding;
    }

    /**
     * Add or sub how much time the player has to discard a building.
     *
     * @param value the value we add.
     */
    public void addOrSubCountDiscardBuilding(int value) {
        countDiscardBuilding += value;
    }

    /**
     * Get how much time the player has to freely move a worker.
     *
     * @return the number of actions.
     */
    public int getCountFreelyMoveWorker() {
        return countFreelyMoveWorker;
    }

    /**
     * Add or sub how much time the player can freely move a worker.
     *
     * @param value the value we add.
     */
    public void addOrSubCountFreelyMoveWorker(int value) {
        countFreelyMoveWorker += value;
    }

    /**
     * Get the turn.
     *
     * @return the number of turn.
     */
    public int getCountTurn() {
        return countTurn;
    }

    /**
     * Get the player.
     *
     * @return the player.
     */
    public Player getPlayer() {
        return player;
    }
}
