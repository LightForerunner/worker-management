/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package display;

import building.Building;
import javafx.scene.shape.Rectangle;
import resources.Worker;

/**
 *
 * @author Gabriel Delleuse
 */
public class Slot extends Rectangle {

    private Worker worker;
    private Building build;

    /**
     * Constructor of Slot.
     *
     * @param x the x-axis.
     * @param y the y-axis.
     * @param w the width.
     * @param h the height.
     */
    public Slot(double x, double y, double w, double h) {
        super(x, y, w, h);
        worker = null;
        build = null;
    }

    /**
     * Get the worker.
     *
     * @return this worker.
     */
    public Worker getWorker() {
        return worker;
    }

    /**
     * Get buillding.
     *
     * @return this building.
     */
    public Building getBuild() {
        return build;
    }

    /**
     * Set the worker.
     *
     * @param worker the worker wanted.
     */
    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    /**
     * Set the worker null.
     */
    public void setWorkerNull() {
        this.worker = null;
    }

    /**
     * Set the building.
     *
     * @param build the building wanted.
     */
    public void setBuild(Building build) {
        this.build = build;
    }

    /**
     * Verify if the slot has a building on it.
     *
     * @return if it's true.
     */
    public boolean hasBuild() {
        return build != null;
    }

    /**
     * Verify if the slot has a building on it.
     *
     * @return if it's true.
     */
    public boolean hasWorker() {
        return worker != null;
    }

    /**
     * Assigns the worker if it exists as the current worker of the game.
     * 
     * @param game the current game.
     */
    public void currentAssignement(GameView game) {
        if (hasWorker()) {
            game.setCurrentWorker(worker);
        }
    }
}
