/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package display;

import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;

/**
 *
 * @author Juline Blanc && Gabriel Delleuse
 */
public class Tools {
    ///////////COLOR///////////
    public static final Color WHITE = Color.rgb(255,255,255);    
    public static final Color LIGHTGREY = Color.rgb(165,165,165);
    public static final Color DARKGREY = Color.rgb(25,25,25);
    
    ///////////RESOLUTION///////////
    public static final double WIDTH = 1024;
    public static final double HEIGHT = 768;
	
    ///////////SHADOW EFFECT///////////
    public static final DropShadow LIGHTSHADOW = new DropShadow(5.0, 3.0, 3.0, Color.LIGHTGREY);
    
    ///////////SHARED INFORMATION///////////
    public static int rectSide = 26;
}
