package building;

import display.GameView;
import display.Slot;
import display.Tools;
import java.util.ArrayList;
import java.util.Random;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import resources.Resources;

/**
 *
 * @author julinblanc && gdelleuse
 */
public class S_Block extends Building {

    protected final static int MAXNUMBER = 30;

    /**
     * Constructor of S_Block.
     */
    public S_Block() {
        Random r = new Random();
        nameBuildings = "Mine";
        slots = new ArrayList<>();
        materialCost = 2;
        production = new Resources(1, 0, 0);
        sense = Sense.HORIZONTAL;
        colBuild = Color.rgb(100 + r.nextInt(155), 100 + r.nextInt(155), 100 + r.nextInt(155));
    }

    /**
     * Another constructor of S_Block.
     *
     * @param group the list which contains all the elements in the interface.
     */
    public S_Block(Group group) {
        slots = new ArrayList<>();
        coordX = Tools.rectSide;
        coordY = 17 * Tools.rectSide;
        sense = Sense.HORIZONTAL;
        colBuild = Tools.DARKGREY;
        displayBuilding(group);
        slots.forEach(s -> s.setStroke(Color.WHITE));
    }

    /**
     * Relocate the building.
     *
     * @param gc the graphics of the canva.
     */
    protected void relocate(GraphicsContext gc) {
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
        gc.setFill(colBuild);
        gc.setStroke(Color.BLACK);
        for (int i = 0; i < 4; i++) {
            if (isSense(Sense.HORIZONTAL)) {
                if (i < 2) {
                    gc.fillRect(coordX + (i + 1) * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + (i + 1) * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX - (i - 3) * Tools.rectSide, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX - (i - 3) * Tools.rectSide, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                }
            } else {
                if (i < 2) {
                    gc.fillRect(coordX, coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX, coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX + Tools.rectSide, coordY + (i - 1) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + Tools.rectSide, coordY + (i - 1) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                }
            }
        }
    }

    /**
     * Display the building.
     *
     * @param gc the graphics of the canva.
     * @param x the coordinate x.
     * @param y the coordinate y.
     */
    @Override
    public void displayBuilding(GraphicsContext gc, double x, double y) {
        coordX = x;
        coordY = y;
        relocate(gc);
    }

    /**
     * Display also the building.
     *
     * @param group the list which contains all the elements in the interface.
     */
    @Override
    protected void displayBuilding(Group group) {
        Slot s = null;
        for (int i = 0; i < 2; i++) {
            s = new Slot(coordX + (i + 1) * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
            s.setFill(colBuild);
            s.setStroke(Color.BLACK);
            group.getChildren().add(s);
            slots.add(s);
        }
        for (int j = 0; j < 2; j++) {
            s = new Slot(coordX - (j - 1) * Tools.rectSide, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
            s.setFill(colBuild);
            s.setStroke(Color.BLACK);
            group.getChildren().add(s);
            slots.add(s);
        }
    }

    /**
     * Place the building in the board.
     *
     * @param s a slot.
     * @param game the game.
     */
    @Override
    public void placement(Slot s, GameView game) {
        if (game.getPlayer().hasMaterials(this)) {
            int iSlot = game.getBoard().getSlots().indexOf(s);
            if (isSense(Sense.HORIZONTAL)) {
                if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot + 20, iSlot + 1, iSlot - 19)) {
                    boardPlacement(game, s, 20, 1, -19);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            } else {
                if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot + 1, iSlot + 21, iSlot + 22)) {
                    boardPlacement(game, s, 1, 21, 22);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            }
        } else {
            System.out.println("You have not enough materials.");
        }
    }

    /**
     * Rotate the building.
     *
     * @param gc the graphics of the canva.
     */
    @Override
    public void rotate(GraphicsContext gc) {
        if (isSense(Sense.HORIZONTAL)) {
            sense = Sense.VERTICAL;
        } else {
            sense = Sense.HORIZONTAL;
        }
        relocate(gc);
    }

    /**
     * Get the max number of the building that can exist in the game.
     *
     * @return the max number.
     */
    public static int getMAXNUMBER() {
        return MAXNUMBER;
    }
}
