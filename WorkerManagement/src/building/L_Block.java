package building;

import display.GameView;
import display.Slot;
import display.Tools;
import java.util.ArrayList;
import java.util.Random;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import player.Player;
import resources.Resources;
import resources.Worker;

/**
 *
 * @author julinblanc && gdelleuse
 */
public class L_Block extends Building {

    protected final static int MAXNUMBER = 20;

    /**
     * Constructor of L-Block.
     */
    public L_Block() {
        Random r = new Random();
        nameBuildings = "Apartment";
        slots = new ArrayList<>();
        materialCost = 8;
        production = new Resources(0, -1, 0);
        sense = Sense.LEFT;
        colBuild = Color.rgb(100 + r.nextInt(155), 100 + r.nextInt(155), 100 + r.nextInt(155));
    }

    /**
     * Another constructor.
     *
     * @param group the list which contains all the elements in the interface.
     */
    public L_Block(Group group) {
        slots = new ArrayList<>();
        coordX = Tools.rectSide;
        coordY = 9 * Tools.rectSide;
        sense = Sense.LEFT;
        colBuild = Tools.DARKGREY;
        displayBuilding(group);
        slots.forEach(s -> s.setStroke(Color.WHITE));
    }

    /**
     * Relocate the building.
     *
     * @param gc the graphics of the canva.
     */
    public void relocate(GraphicsContext gc) {
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
        gc.setFill(colBuild);
        gc.setStroke(Color.BLACK);
        for (int i = 0; i < 4; i++) {
            if (isSense(Sense.LEFT)) {
                if (i < 2) {
                    gc.fillRect(coordX, coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX, coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX + (i - 2) * Tools.rectSide, coordY + 2 * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + (i - 2) * Tools.rectSide, coordY + 2 * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                }
            } else if (isSense(Sense.UP)) {
                if (i < 2) {
                    gc.fillRect(coordX + (i + 1) * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + (i + 1) * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX, coordY + (i - 2) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX, coordY + (i - 2) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                }
            } else if (isSense(Sense.RIGHT)) {
                if (i < 2) {
                    gc.fillRect(coordX + i * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + i * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX + Tools.rectSide, coordY + (i - 1) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + Tools.rectSide, coordY + (i - 1) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                }
            } else {
                if (i < 2) {
                    gc.fillRect(coordX + i * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + i * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX + 2 * Tools.rectSide, coordY - (i - 2) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + 2 * Tools.rectSide, coordY - (i - 2) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                }
            }
        }
    }

    /**
     * Display the building.
     *
     * @param gc the graphics of the canva.
     * @param x the coordinate x.
     * @param y the coordinate y.
     */
    @Override
    public void displayBuilding(GraphicsContext gc, double x, double y) {
        coordX = x;
        coordY = y;
        relocate(gc);
    }

    /**
     * Display also the building.
     *
     * @param group the list which contains all the elements in the interface.
     */
    @Override
    protected void displayBuilding(Group group) {
        Slot s = null;
        for (int i = 0; i < 2; i++) {
            s = new Slot(coordX, coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
            s.setFill(colBuild);
            s.setStroke(Color.BLACK);
            group.getChildren().add(s);
            slots.add(s);
        }
        for (int i = 0; i < 2; i++) {
            s = new Slot(coordX + i * Tools.rectSide, coordY + 2 * Tools.rectSide, Tools.rectSide, Tools.rectSide);
            s.setFill(colBuild);
            s.setStroke(Color.BLACK);
            group.getChildren().add(s);
            slots.add(s);
        }
    }

    /**
     * Place the building in the board.
     *
     * @param s the slot.
     * @param game the game.
     */
    @Override
    public void placement(Slot s, GameView game) {
        if (game.getPlayer().hasMaterials(this)) {
            int iSlot = game.getBoard().getSlots().indexOf(s);
            int colPass = game.getBoard().getRows();
            if (isSense(Sense.LEFT)) {
                if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot + 1, iSlot + 2, iSlot + colPass + 2)) {
                    boardPlacement(game, s, 1, 2, colPass + 2);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            } else if (isSense(Sense.UP)) {
                if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot + colPass, iSlot + colPass * 2, iSlot + 1)) {
                    boardPlacement(game, s, colPass, colPass * 2, 1);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            } else if (isSense(Sense.RIGHT)) {
                if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot + colPass, iSlot + colPass + 1, iSlot + colPass + 2)) {
                    boardPlacement(game, s, colPass, colPass + 1, colPass + 2);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            } else {
                if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot + 1, iSlot - 19, iSlot - 39)) {
                    boardPlacement(game, s, 1, -19, -39);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            }
        } else {
            System.out.println("You have not enough materials.");
        }
    }

    /**
     * Rotate the building.
     *
     * @param gc the graphics of the canva.
     */
    @Override
    public void rotate(GraphicsContext gc) {
        if (isSense(Sense.LEFT)) {
            sense = Sense.UP;
        } else if (isSense(Sense.UP)) {
            sense = Sense.RIGHT;
        } else if (isSense(Sense.RIGHT)) {
            sense = Sense.DOWN;
        } else {
            sense = Sense.LEFT;
        }
        relocate(gc);
    }

    /**
     * Get the max number of the building that can exist in the game.
     *
     * @return the max number.
     */
    public static int getMAXNUMBER() {
        return MAXNUMBER;
    }

    @Override
    public String productionToString() {
        return "Cost of placement : "
                + materialCost + " Materials."
                + "\nProduces : "
                + production.getEnergies().getQuantity() + " Energy, "
                + production.getMaterials().getQuantity() + " Material.\n"
                + "For each two workers on it, \n"
                + "returns them to you, and add\n"
                + "one worker to your pile.";
    }

    @Override
    public void product(Player player) {
        int nbAction = 0;
        nbAction = workerSlots.size() / 2;
        System.out.println(nbAction);
        for (int i = 0; i < nbAction; i++) {
            System.out.println(i);
            player.getResources().addOrSubEnergies(production.getEnergies().getQuantity());
            player.addWorker(workerSlots.get(workerSlots.size() - 1));
            player.getGroup().getChildren()
                    .remove(workerSlots.get(workerSlots.size() - 1).getDisplay());
            workerSlots.remove(workerSlots.size() - 1);
            player.addWorker(workerSlots.get(workerSlots.size() - 1));
            player.getGroup().getChildren()
                    .remove(workerSlots.get(workerSlots.size() - 1).getDisplay());
            workerSlots.remove(workerSlots.size() - 1);
            player.addWorker(new Worker());
        }
    }
}
