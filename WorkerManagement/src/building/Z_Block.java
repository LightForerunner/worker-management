package building;

import display.GameView;
import display.Slot;
import display.Tools;
import java.util.ArrayList;
import java.util.Random;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import resources.Resources;

/**
 *
 * @author julinblanc && gdelleuse
 */
public class Z_Block extends Building {

    protected final static int MAXNUMBER = 10;

    /**
     * Constructor of Z_Block.
     */
    public Z_Block() {
        Random r = new Random();
        nameBuildings = "Junkyard";
        slots = new ArrayList<>();
        materialCost = 2;
        production = new Resources(0, -1, 0);
        //Production is creating a new building in the pile.
        sense = Sense.HORIZONTAL;
        colBuild = Color.rgb(100 + r.nextInt(155), 100 + r.nextInt(155), 100 + r.nextInt(155));
    }

    /**
     * Another constructor for a shadow Z Block.
     *
     * @param group the list which contains all the elements in the interface.
     */
    public Z_Block(Group group) {
        slots = new ArrayList<>();
        coordX = Tools.rectSide;
        coordY = 24 * Tools.rectSide;
        sense = Sense.HORIZONTAL;
        colBuild = Tools.DARKGREY;
        displayBuilding(group);
        slots.forEach(s -> s.setStroke(Color.WHITE));
    }

    /**
     * Relocate the building.
     *
     * @param gc the graphics of the canva.
     */
    protected void relocate(GraphicsContext gc) {
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
        gc.setFill(colBuild);
        gc.setStroke(Color.BLACK);
        for (int i = 0; i < 4; i++) {
            if (isSense(Sense.HORIZONTAL)) {
                if (i < 2) {
                    gc.fillRect(coordX + i * Tools.rectSide, 
                            coordY, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + i * Tools.rectSide, 
                            coordY, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX + (i - 1) * Tools.rectSide, 
                            coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + (i - 1) * Tools.rectSide, 
                            coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                }
            } else {
                if (i < 2) {
                    gc.fillRect(coordX + Tools.rectSide, 
                            coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + Tools.rectSide, 
                            coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX, coordY + (i - 1) * Tools.rectSide, 
                            Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX, coordY + (i - 1) * Tools.rectSide,
                            Tools.rectSide, Tools.rectSide);
                }
            }
        }
    }

    /**
     * Display the building.
     *
     * @param gc the graphics of the canva.
     * @param x the coordinate x.
     * @param y the coordinate y.
     */
    @Override
    public void displayBuilding(GraphicsContext gc, double x, double y) {
        coordX = x;
        coordY = y;
        relocate(gc);
    }

    /**
     * Display the building.
     *
     * @param group the list which contains all the elements in the interface.
     */
    @Override
    protected void displayBuilding(Group group) {
        Slot s = null;
        for (int i = 0; i < 2; i++) {
            s = new Slot(coordX + i * Tools.rectSide, 
                    coordY, Tools.rectSide, Tools.rectSide);
            s.setFill(colBuild);
            s.setStroke(Color.BLACK);
            group.getChildren().add(s);
            slots.add(s);
        }
        for (int i = 0; i < 2; i++) {
            s = new Slot(coordX + (i + 1) * Tools.rectSide, 
                    coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
            s.setFill(colBuild);
            s.setStroke(Color.BLACK);
            group.getChildren().add(s);
            slots.add(s);
        }
    }

    /**
     * Place the building in the board.
     *
     * @param s a slot.
     * @param game the game.
     */
    @Override
    public void placement(Slot s, GameView game) {
        if (game.getPlayer().hasMaterials(this)) {
            int iSlot = game.getBoard().getSlots().indexOf(s);
            if (isSense(Sense.HORIZONTAL)) {
                if (game.getBoard()
                        .isNotOutOfVerticalBonds(iSlot, iSlot + 20, iSlot + 21, iSlot + 41)) {
                    boardPlacement(game, s, 20, 21, 41);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            } else {
                if (game.getBoard()
                        .isNotOutOfVerticalBonds(iSlot, iSlot + 1, iSlot - 19, iSlot - 18)) {
                    boardPlacement(game, s, 1, -19, -18);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            }
        } else {
            System.out.println("You have not enough materials.");
        }
    }

    /**
     * Rotate the building.
     *
     * @param gc the graphics of the canva.
     */
    @Override
    public void rotate(GraphicsContext gc) {
        if (isSense(Sense.HORIZONTAL)) {
            sense = Sense.VERTICAL;
        } else {
            sense = Sense.HORIZONTAL;
        }
        relocate(gc);
    }

    /**
     * Get the max number of the building
     * that can exist in the game.
     *
     * @return the max number.
     */
    public static int getMAXNUMBER() {
        return MAXNUMBER;
    }

    @Override
    public String productionToString() {
        return "Cost of placement : "
                + materialCost + " Materials."
                + "\nProduces : "
                + production.getEnergies().getQuantity() + " Energy, "
                + production.getMaterials().getQuantity() + " Material.\n"
                + "Discards a building chosen from\n"
                + "the player's deck and replace it\n"
                + "by another random building.";
    }
}
