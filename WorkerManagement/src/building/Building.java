package building;

import display.GameView;
import display.Slot;
import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import player.Player;
import resources.Resources;
import resources.Worker;

/**
 * The base of each building class.
 *
 * @author julinblanc && gdelleuses
 */
public abstract class Building {

    protected enum Sense {
        UP, DOWN, LEFT, RIGHT, HORIZONTAL, VERTICAL
    };
    protected ArrayList<Slot> slots;
    protected Color colBuild;
    protected Resources production;
    protected Sense sense;
    protected String nameBuildings;
    protected ArrayList<Worker> workerSlots = new ArrayList<>();
    protected double coordX, coordY;
    protected int materialCost;

    /**
     * Get the slots.
     *
     * @return display.
     */
    public ArrayList<Slot> getSlots() {
        return slots;
    }

    /**
     * Get the workers in the building.
     *
     * @return a table with workers.
     */
    public ArrayList<Worker> getWorkerSlots() {
        return workerSlots;
    }

    /**
     * Get the name of the building.
     *
     * @return the name.
     */
    public String getNameBuilding() {
        return nameBuildings;
    }

    /**
     * Get the color of the building.
     *
     * @return the color.
     */
    public Color getColBuild() {
        return colBuild;
    }

    /**
     * Get the coordinate x.
     *
     * @return the coordinate.
     */
    public double getCoordX() {
        return coordX;
    }

    /**
     * Get the coordinates y.
     *
     * @return the coordinate.
     */
    public double getCoordY() {
        return coordY;
    }

    /**
     * Get the orientation of the building.
     *
     * @return the sense.
     */
    public Sense getSense() {
        return sense;
    }

    /**
     * Set the sense of the building
     *
     * @param sense the given sense.
     */
    public void setSense(Sense sense) {
        this.sense = sense;
    }

    /**
     * Verify if the given sense is like the sense of the building.
     *
     * @param s the given sense.
     * @return if it's true.
     */
    public boolean isSense(Sense s) {
        return sense.equals(s);
    }

    /**
     * Get the material cost of the building.
     *
     * @return the cost.
     */
    public int getMaterialCost() {
        return materialCost;
    }

    /**
     * Get the production of the building.
     *
     * @return the production.
     */
    public Resources getProduction() {
        return production;
    }

    /**
     * Get the class of the building.
     *
     * @return the class.
     */
    public String getBuildClass() {
        return getClass().getName().substring(9);
    }

    /**
     * Get the production in a string.
     *
     * @return the production.
     */
    public String productionToString() {
        return "Cost of placement : "
                + materialCost + " Materials."
                + "\nProduces : "
                + production.getEnergies().getQuantity() + " Energy, "
                + production.getMaterials().getQuantity() + " Material.";
    }

    /**
     * Add or sub the ressources.
     *
     * @param player the concerned player.
     */
    public void product(Player player) {
        if (player.hasResources(this)) {
            player.payResources(this);
        }
    }

    /**
     * Sets the building appearance in the selected Building window.
     *
     * @param gc the graphics of the canva.
     * @param x the coordinate.
     * @param y the other coordinate.
     */
    public abstract void displayBuilding(GraphicsContext gc, double x, double y);

    /**
     * Sets the shadow building appearance.
     *
     * @param group the list which contains all the elements in the interface.
     */
    protected abstract void displayBuilding(Group group);

    /**
     * Sets the building appearance.
     *
     * @param gc the graphics of the canva.
     */
    public abstract void rotate(GraphicsContext gc);

    /**
     * Place the build in the selected slot.
     *
     * @param s a slot.
     * @param game the current game.
     */
    public abstract void placement(Slot s, GameView game);

    /**
     * Place this on the game board.
     *
     * @param game the current game.
     * @param s the slot clicked when called.
     * @param secondPos the coord of the second Pos
     * @param thirdPos the coord of the third Pos
     * @param fourthPos the coord of the fourth Pos
     */
    protected void boardPlacement(GameView game, Slot s,
            int secondPos, int thirdPos, int fourthPos) {
        int iSlot = game.getBoard().getSlots().indexOf(s);
        s.setFill(colBuild);
        s.setBuild(this);
        game.getBoard().getSlots().get(iSlot + secondPos).setFill(colBuild);
        game.getBoard().getSlots().get(iSlot + secondPos).setBuild(this);
        game.getBoard().getSlots().get(iSlot + thirdPos).setFill(colBuild);
        game.getBoard().getSlots().get(iSlot + thirdPos).setBuild(this);
        game.getBoard().getSlots().get(iSlot + fourthPos).setFill(colBuild);
        game.getBoard().getSlots().get(iSlot + fourthPos).setBuild(this);
        game.decrementPlaceBuilding();
        System.out.println("Building placed.");
    }
}
