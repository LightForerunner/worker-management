package building;

import display.GameView;
import display.Slot;
import display.Tools;
import java.util.ArrayList;
import java.util.Random;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import resources.Resources;

/**
 *
 * @author julinblanc && gdelleuse
 */
public class T_Block extends Building {

    protected final static int MAXNUMBER = 30;

    /**
     * Constructor of T_Block.
     */
    public T_Block() {
        Random r = new Random();
        nameBuildings = "Solar Panel";
        slots = new ArrayList<>();
        materialCost = 2;
        production = new Resources(0, 2, 0);
        sense = Sense.UP;
        colBuild = Color.rgb(100 + r.nextInt(155), 100 + r.nextInt(155), 100 + r.nextInt(155));
    }

    /**
     * Another constructor of T_Block.
     *
     * @param group the list which contains all the elements in the interface.
     */
    public T_Block(Group group) {
        slots = new ArrayList<>();
        coordX = Tools.rectSide;
        coordY = 20.5 * Tools.rectSide;
        sense = Sense.UP;
        colBuild = Tools.DARKGREY;
        displayBuilding(group);
        slots.forEach(s -> s.setStroke(Color.WHITE));
    }

    /**
     * Display the building.
     *
     * @param gc the graphics of the canva.
     * @param x the coordinate x.
     * @param y the coordinate y.
     */
    @Override
    public void displayBuilding(GraphicsContext gc, double x, double y) {
        coordX = x;
        coordY = y;
        relocate(gc);
    }

    /**
     * Display the building starting on the top.
     *
     * @param group the list which contains all the elements in the interface.
     */
    @Override
    protected void displayBuilding(Group group) {
        Slot s = null;
        s = new Slot(coordX + Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
        s.setFill(colBuild);
        s.setStroke(Color.BLACK);
        group.getChildren().add(s);
        slots.add(s);
        for (int i = 0; i < 3; i++) {
            s = new Slot(coordX + i * Tools.rectSide, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
            s.setFill(colBuild);
            s.setStroke(Color.BLACK);
            group.getChildren().add(s);
            slots.add(s);
        }
    }

    /**
     * Relocate the building.
     *
     * @param gc the graphics of the canva.
     */
    protected void relocate(GraphicsContext gc) {
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
        gc.setFill(colBuild);
        gc.setStroke(Color.BLACK);
        for (int i = 0; i < 4; i++) {
            if (isSense(Sense.UP)) {
                if (i < 1) {
                    gc.fillRect(coordX + Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX + (i - 1) * Tools.rectSide, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + (i - 1) * Tools.rectSide, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                }
            } else if (isSense(Sense.RIGHT)) {
                if (i < 1) {
                    gc.fillRect(coordX + Tools.rectSide, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + Tools.rectSide, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX, coordY + (i - 1) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX, coordY + (i - 1) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                }
            } else if (isSense(Sense.DOWN)) {
                if (i < 1) {
                    gc.fillRect(coordX + Tools.rectSide, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + Tools.rectSide, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX + (i - 1) * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + (i - 1) * Tools.rectSide, coordY, Tools.rectSide, Tools.rectSide);
                }
            } else {
                if (i < 1) {
                    gc.fillRect(coordX, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX, coordY + Tools.rectSide, Tools.rectSide, Tools.rectSide);
                } else {
                    gc.fillRect(coordX + Tools.rectSide, coordY + (i - 1) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                    gc.strokeRect(coordX + Tools.rectSide, coordY + (i - 1) * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                }
            }
        }
    }

    /**
     * Place the building in the board.
     *
     * @param s the slot.
     * @param game the game.
     */
    @Override
    public void placement(Slot s, GameView game) {
        if (game.getPlayer().hasMaterials(this)) {
            int iSlot = game.getBoard().getSlots().indexOf(s);
            if (isSense(Sense.LEFT)) {
                if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot + 19, iSlot + 20, iSlot + 21)) {
                    boardPlacement(game, s, 19, 20, 21);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            } else if (isSense(Sense.UP)) {
                if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot - 19, iSlot + 1, iSlot + 21)) {
                    boardPlacement(game, s, -19, 1, 21);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            } else if (isSense(Sense.RIGHT)) {
                if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot + 1, iSlot + 2, iSlot + 21)) {
                    boardPlacement(game, s, 1, 2, 21);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            } else {
                if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot + 20, iSlot + 40, iSlot + 21)) {
                    boardPlacement(game, s, 20, 40, 21);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            }
        } else {
            System.out.println("You have not enough materials.");
        }
    }

    /**
     * Rotate the building.
     *
     * @param gc the graphics of the canva.
     */
    @Override
    public void rotate(GraphicsContext gc) {
        if (isSense(Sense.UP)) {
            sense = Sense.RIGHT;
        } else if (isSense(Sense.RIGHT)) {
            sense = Sense.DOWN;
        } else if (isSense(Sense.DOWN)) {
            sense = Sense.LEFT;
        } else {
            sense = Sense.UP;
        }
        relocate(gc);
    }

    /**
     * Get the max number of the building that can exist in the game.
     *
     * @return the max number.
     */
    public static int getMAXNUMBER() {
        return MAXNUMBER;
    }
}
