package building;

import display.GameView;
import display.Slot;
import display.Tools;
import java.util.ArrayList;
import java.util.Random;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import resources.Resources;

/**
 *
 * @author julinblanc && gdelleuse
 */
public class I_Block extends Building {

    protected final static int MAXNUMBER = 25;

    /**
     * Constructor of I_Block.
     */
    public I_Block() {
        Random r = new Random();
        nameBuildings = "Factory";
        slots = new ArrayList<>();
        materialCost = 4;
        production = new Resources(2, -1, 0);
        sense = Sense.HORIZONTAL;
        colBuild = Color
                .rgb(100 + r.nextInt(155), 100 + r.nextInt(155), 100 + r.nextInt(155));
    }

    /**
     * Another constructor.
     *
     * @param group the list which contains all the elements in the interface.
     */
    public I_Block(Group group) {
        slots = new ArrayList<>();
        coordX = Tools.rectSide;
        coordY = 1.5 * Tools.rectSide;
        sense = Sense.HORIZONTAL;
        colBuild = Tools.DARKGREY;
        displayBuilding(group);
        slots.forEach(s -> s.setStroke(Color.WHITE));
    }

    /**
     * Relocate the building.
     *
     * @param gc the graphics of the canva.
     */
    protected void relocate(GraphicsContext gc) {
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), 
                gc.getCanvas().getHeight());
        gc.setFill(colBuild);
        gc.setStroke(Color.BLACK);
        for (int i = 0; i < 4; i++) {
            if (isSense(Sense.HORIZONTAL)) {
                gc.fillRect(coordX + i * Tools.rectSide, 
                        coordY, Tools.rectSide, Tools.rectSide);
                gc.strokeRect(coordX + i * Tools.rectSide, 
                        coordY, Tools.rectSide, Tools.rectSide);
            } else {
                gc.fillRect(coordX, 
                        coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                gc.strokeRect(coordX, 
                        coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
            }
        }
    }

    /**
     * Display the building.
     *
     * @param gc the graphics of the canva.
     * @param x the coordinate x.
     * @param y the coordinate y.
     */
    @Override
    public void displayBuilding(GraphicsContext gc, 
            double x, double y) {
        coordX = x;
        coordY = y;
        relocate(gc);
    }

    /**
     * Display also the building.
     *
     * @param group the list which contains all the elements in the interface.
     */
    @Override
    protected void displayBuilding(Group group) {
        Slot s = null;
        slots = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            if (isSense(Sense.HORIZONTAL)) {
                s = new Slot(coordX + i * Tools.rectSide,
                        coordY, Tools.rectSide, Tools.rectSide);
                s.setFill(colBuild);
                s.setStroke(Color.BLACK);
                group.getChildren().add(s);
                slots.add(s);
            } else {
                s = new Slot(coordX, 
                        coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                s.setFill(colBuild);
                s.setStroke(Color.BLACK);
                group.getChildren().add(s);
                slots.add(s);
            }
        }
    }

    /**
     * Place the build in the specified slots array.
     *
     * @param s a slot.
     * @param game the game.
     */
    @Override
    public void placement(Slot s, GameView game) {
        if (game.getPlayer().hasMaterials(this)) {
            int iSlot = game.getBoard().getSlots().indexOf(s);
            int colPass = game.getBoard().getRows();
            if (isSense(Sense.HORIZONTAL)) {
                if (game.getBoard()
                        .isNotOutOfVerticalBonds(iSlot, iSlot + colPass, iSlot + colPass * 2, iSlot + colPass * 3)) {
                    boardPlacement(game, s, colPass, colPass * 2, colPass * 3);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            } else {
                if (game.getBoard()
                        .isNotOutOfVerticalBonds(iSlot, iSlot + 1, iSlot + 2, iSlot + 3)) {
                    boardPlacement(game, s, 1, 2, 3);
                    game.getPlayer().payCost(this);
                    game.clearGame(this);
                }
            }
        } else {
            System.out.println("You have not enough materials.");
        }
    }

    /**
     * Relocate the building.
     *
     * @param gc the graphics of the canva.
     */
    @Override
    public void rotate(GraphicsContext gc) {
        if (isSense(Sense.HORIZONTAL)) {
            sense = Sense.VERTICAL;
        } else {
            sense = Sense.HORIZONTAL;
        }
        relocate(gc);
    }

    /**
     * Get the max number of the building
     * that can exist in the game.
     *
     * @return the max number.
     */
    public static int getMAXNUMBER() {
        return MAXNUMBER;
    }
}
