package building;

import display.GameView;
import display.Slot;
import display.Tools;
import java.util.ArrayList;
import java.util.Random;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import resources.Resources;

/**
 *
 * @author julinblanc && gdelleuse
 */
public class O_Block extends Building {

    protected final static int MAXNUMBER = 10;

    /**
     * Constructor O_Block.
     */
    public O_Block() {
        Random r = new Random();
        nameBuildings = "Park";
        slots = new ArrayList<>();
        materialCost = 8;
        production = new Resources(0, 0, 0);
        // production is allowing the player to move freely a worker.
        colBuild = Color.rgb(100 + r.nextInt(155), 100 + r.nextInt(155), 100 + r.nextInt(155));
    }

    /**
     * Another constructor.
     *
     * @param group the list which contains all the elements in the interface.
     */
    public O_Block(Group group) {
        slots = new ArrayList<>();
        coordX = Tools.rectSide;
        coordY = 13.5 * Tools.rectSide;
        colBuild = Tools.DARKGREY;
        displayBuilding(group);
        slots.forEach(s -> s.setStroke(Color.WHITE));
    }

    /**
     * Relocate the building.
     *
     * @param gc the graphics of the canva.
     */
    protected void relocate(GraphicsContext gc) {
        //Nothing to do with O_Block
    }

    /**
     * Display the building.
     *
     * @param gc the graphics of the canva.
     * @param x
     * @param y
     */
    @Override
    public void displayBuilding(GraphicsContext gc, double x, double y) {
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
        coordX = x;
        coordY = y;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                gc.setFill(colBuild);
                gc.setStroke(Color.BLACK);
                gc.fillRect(coordX + j * Tools.rectSide, coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                gc.strokeRect(coordX + j * Tools.rectSide, coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
            }
        }
    }

    /**
     * Display also the building.
     *
     * @param group the list which contains all the elements in the interface.
     */
    @Override
    protected void displayBuilding(Group group) {
        Slot s = null;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                s = new Slot(coordX + j * Tools.rectSide, coordY + i * Tools.rectSide, Tools.rectSide, Tools.rectSide);
                s.setFill(colBuild);
                s.setStroke(Color.BLACK);
                group.getChildren().add(s);
                slots.add(s);
            }
        }
    }

    /**
     * Place the building in the board.
     *
     * @param s a slot.
     * @param game the game.
     */
    @Override
    public void placement(Slot s, GameView game) {
        if (game.getPlayer().hasMaterials(this)) {
            int iSlot = game.getBoard().getSlots().indexOf(s);
            if (game.getBoard().isNotOutOfVerticalBonds(iSlot, iSlot + 1, iSlot + 20, iSlot + 21)) {
                boardPlacement(game, s, 1, 20, +21);
                game.getPlayer().payCost(this);
                game.clearGame(this);
            }
        } else {
            System.out.println("You have not enough materials.");
        }
    }

    /**
     * Rotate a building, useless in the case of O_Block.
     *
     * @param gc the graphics of the canva.
     */
    @Override
    public void rotate(GraphicsContext gc) {
        //nothing to do
    }

    /**
     * Get the max number of the building that can exist in the game.
     *
     * @return the max number.
     */
    public static int getMAXNUMBER() {
        return MAXNUMBER;
    }

    @Override
    public String productionToString() {
        return "Cost of placement : "
                + materialCost + " Materials."
                + "\nProduces : nothing but allows\n"
                + "you to move any one\n"
                + "worker to another slot.";
    }
}
